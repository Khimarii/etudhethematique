<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

use App\Entity\Pointage;
use App\Entity\Formation;
use App\Entity\Cours;
use App\Entity\Utilisateur;
use App\Form\PointageFiltreType;

date_default_timezone_set('Europe/Paris');

class PanelProfController extends AbstractController
{
		public function pointageViewerTeacher(Request $request, EntityManagerInterface $em)
			{
					$listeformation = $em->getRepository(Formation::class)->findAll();
					$listecours = $em->getRepository(Cours::class)->findAll();
	//				$formation = new Formation();
	//				$newpointage = new Pointage();

					$form = $this->createForm(PointageFiltreType::class, null, ['formations' => $listeformation, 'cours' => $listecours]);
					$form->handleRequest($request);
					if ($form->isSubmitted() && $form->isValid()) {
						$cours = $form['cours']->getData();
						$listeDePointage = $em->getRepository(Pointage::class)->findPointageByUEForToday($em, $cours->getNomUe());
						$newpointage = new Pointage();
						if ($listeDePointage != null)
						{
							$newpointage = $listeDePointage[0];
							$nbpointage = count($listeDePointage);

							return $this->render('pointageViewerTeacher.html.twig', [
									'form' => $form->createView(), 'listeDePointage' => $listeDePointage, 'newpointage' => $newpointage, 'nbpointage' => $nbpointage
							]);
						}
					}

					return $this->render('pointageViewerTeacher.html.twig', [
							'form' => $form->createView(), 'listeDePointage' => null, 'newpointage' => null, 'nbpointage' => null
					]);
			}
}
