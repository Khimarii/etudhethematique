<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\Session\Session;

use App\Entity\Pointage;
use App\Entity\Utilisateur;

use DateTimeImmutable;

class PanelEtudiantController extends AbstractController
{
  public function panelEtudiant()
  {
    $user = $this->getUser();
    $utilisateurEtudiant = $this->getDoctrine()->getRepository(Utilisateur::class)->find($user);
    $idPointage = $this->getDoctrine()->getRepository(Pointage::class)->findPointageSortieByEtudiant($utilisateurEtudiant);
    if($idPointage != null){
      $pointageExistant = $this->getDoctrine()->getRepository(Pointage::class)->find($idPointage[0]);
      $cours = $pointageExistant->getCours();
      return $this->render('panelEtudiant.html.twig', array('message' => 'Vous avez pointé en entrée de cours :', 'cours' => $cours, 'prenom' => $user->getPrenomUtilisateur(), 'nom' => $user->getNomUtilisateur()));
    } else {
      return $this->render('panelEtudiant.html.twig', array('message' => 'Vous n\'avez pas de pointage pour le moment', 'cours' =>'', 'prenom' => $user->getPrenomUtilisateur(), 'nom' => $user->getNomUtilisateur()));
    }
  }

  public function showPointageWeek(EntityManagerInterface $em)
  {
    $user = $this->getUser();
    $date = new \DateTime();
    $dateFinSemaine = new \DateTime();
    $date = $date->setTimestamp(strtotime('monday this week'));
    $dateFinSemaine = $dateFinSemaine->setTimestamp(strtotime('sunday this week'));
    $listeDePointage = $em->getRepository(Pointage::class)->findPointageByWeek($em, $user , $date, $dateFinSemaine);
    //{Conversion duree et retard en H/m et cumul des heures et des retards sur la semaine
    $cumulHeure = array('heures'=>0, 'minutes'=>0, 'secondes'=>0);
    $cumulRetard = array('heures'=>0, 'minutes'=>0, 'secondes'=>0);
    foreach ($listeDePointage as &$value) {
      $heurePointage = intval($value['dureePointage']/3600);
      $minutesPointage = intval(($value['dureePointage']%3600)/60);
      $secondesPointage = intval(($value['dureePointage']%3600)%60);
      $heureRetard = intval($value['retard']/3600);
      $minutesRetard = intval(($value['retard']%3600)/60);
      $secondesRetard = intval(($value['retard']%3600)%60);
      $cumulHeure = $this->additionHeures($cumulHeure, $heurePointage, $minutesPointage, $secondesPointage);
      $cumulRetard = $this->additionHeures($cumulRetard, $heureRetard, $minutesRetard, $secondesRetard);
    //}
    }
    return $this->render('pointageSemaine.html.twig', array('listeDePointage' => $listeDePointage, 'compteurHeureSemaine'=> $cumulHeure, 'compteurRetardSemaine' => $cumulRetard, 'prenom' => $user->getPrenomUtilisateur(), 'nom' => $user->getNomUtilisateur()));
  }
  private function additionHeures($tab, $h, $m, $s){
    $tab['heures'] = $tab['heures']+$h;
    if($tab['minutes']+$m > 60){
      $tab['minutes']=$tab['minutes']+$m-60;
      $tab['heures']= $tab['heures']+1;
    }else{
      $tab['minutes']=$tab['minutes']+$m;
    }
    if($tab['secondes']+$s > 60){
      $tab['secondes']=$tab['secondes']+$s-60;
      $tab['minutes']= $tab['minutes']+1;
    }else{
      $tab['secondes']=$tab['secondes']+$s;
    }
    return $tab;
  }
  // Fonction pour additioner les heures effectuées sur la semaine par l'étudiant
    // private function recupHeures($listePointage){
    //   $cumulHeure = array('heures'=>0, 'minutes'=>0, 'secondes'=>0);
    //   foreach ($listePointage as &$value) {
    //     $temps = date_diff($value['datePointageEntree'], $value['datePointageSortie']);
    //     $heures = intval($temps->format('%H'));
    //     $minutes = intval($temps->format('%I'));
    //     $secondes = intval($temps->format('%S'));
    //     $cumulHeure = $this->additionHeures($cumulHeure, $heures, $minutes, $secondes);
    //   }
    //   return $cumulHeure;
    // }

    //switch pour toujours partir de lundi et finir à samedi quelque soit le jour de visualisation des pointages
    // $jour = date('w');
    // switch($jour){
    //   case 1:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d'), date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')+6, date('Y')));
    //     break;
    //   case 2:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')-1, date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')+5, date('Y')));
    //     break;
    //   case 3:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')-2, date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')+4, date('Y')));
    //     break;
    //   case 4:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')-3, date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')+3, date('Y')));
    //     break;
    //   case 5:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')-4, date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')+2, date('Y')));
    //     break;
    //   case 6:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')-5, date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')+1, date('Y')));
    //     break;
    //   case 0:
    //     $date = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d')-6, date('Y')));
    //     $dateFinSemaine = date('Y-m-d',mktime(0 ,0 ,0 , date('m'), date('d'), date('Y')));
    //     break;
    // }
}
